# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-08-31 22:19+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: paperwork-shell/src/paperwork_shell/main.py:90
msgid "command"
msgstr ""

#: paperwork-shell/src/paperwork_shell/display/scan.py:91
msgid "Scanning page {} (expected size: {}x{}) ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/display/scan.py:132
msgid "Page {} scanned (actual size: {}x{})"
msgstr ""

#: paperwork-shell/src/paperwork_shell/display/scan.py:139
msgid "End of paper feed"
msgstr ""

#: paperwork-shell/src/paperwork_shell/display/scan.py:155
msgid "Page {} in document {} created"
msgstr ""

#: paperwork-shell/src/paperwork_shell/display/docrendering/extra_text.py:32
#: paperwork-shell/src/paperwork_shell/cmd/extra_text.py:71
msgid "Additional text:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/ocr.py:44
msgid "OCR document or pages"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/ocr.py:49
msgid "Document on which OCR must be run"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/ocr.py:54
msgid ""
"Pages to OCR (single integer, range or comma-separated list, default: all "
"pages)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/ocr.py:83
#, python-brace-format
msgid "Running OCR on document {doc_id} page {page_idx} ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/ocr.py:92
#: paperwork-shell/src/paperwork_shell/cmd/export.py:244
#: paperwork-shell/src/paperwork_shell/cmd/reset.py:118
#: paperwork-shell/src/paperwork_shell/cmd/label.py:91
#: paperwork-shell/src/paperwork_shell/cmd/edit.py:169
#: paperwork-shell/src/paperwork_shell/cmd/import.py:156
#: paperwork-shell/src/paperwork_shell/cmd/extra_text.py:80
msgid "Done"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/ocr.py:96
#: paperwork-shell/src/paperwork_shell/cmd/reset.py:119
#: paperwork-shell/src/paperwork_shell/cmd/edit.py:170
#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:112
#: paperwork-shell/src/paperwork_shell/cmd/sync.py:73
msgid "All done !"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:57
msgid "Manage scanner configuration"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:60
#: paperwork-shell/src/paperwork_shell/cmd/extra_text.py:41
msgid "sub-command"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:64
msgid "List all scanners and their possible settings"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:69
msgid "Show the currently selected scanner and its settings"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:74
msgid "Define which scanner and which settings to use"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:77
msgid "Scanner to use"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:82
msgid ""
"Default source on the scanner to use (if not specified, one will be selected "
"randomly)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:88
msgid "Default resolution (dpi ; default=300)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:92
msgid "Examining scanner {} ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:149
#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:171
msgid "ID:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:151
#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:172
msgid "Source:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:153
msgid "Resolutions:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:173
msgid "Resolution:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:200
msgid "Source {} not found on device. Using another source"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:219
msgid "Default source:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scanner.py:233
msgid "Resolution {} not available. Adjusted to {}."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/about/__init__.py:82
msgid "Version: "
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/about/__init__.py:86
msgid "Because sorting documents is a machine's job."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/about/__init__.py:160
msgid "About Paperwork"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/rename.py:45
msgid "Change a document identifier"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/rename.py:49
msgid "Document to rename"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/rename.py:53
msgid "New name for the document"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:62
msgid ""
"Export a document, a page, or a set of pages. Example: paperwork-cli export "
"20150303_2314_39 -p 2 -f img_boxes -f grayscale -f jpeg -o ~/tmp/pouet.jpg"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:68
msgid "Document to export"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:72
msgid ""
"Pages to export (single integer, range or comma-separated list, default: all "
"pages)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:81
msgid ""
"Export filters. Specify this option once for each filter to apply (ex: '-f "
"grayscale -f jpeg')."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:88
msgid ""
"Output file/directory. If not specified, will list the filters that could be "
"chained after those already specified."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:97
msgid "Next possible filters are:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:105
msgid "a document list"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:107
msgid "a document"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:109
msgid "pages"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:111
msgid "Images and text boxes"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:127
msgid "Need at least one filter."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:131
msgid "First filter cannot be '{}'"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:144
#, python-brace-format
msgid "Filter mismatch: {0} expects {1} as input. Got {2} instead"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:160
msgid "Last filter will output {}."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:201
#, python-format
msgid "Unknown filters: %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:206
msgid ""
"Run the command without any filter to have a list of possible filters to "
"start with."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:220
msgid "Filter list is complete, but no output file specified (-o)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:240
#, python-format
msgid "Exporting to %s ... "
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/export.py:247
msgid "Export failed !"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/reset.py:67
msgid "Reset a page to its original content"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/reset.py:101
msgid "Reseting document {} page {} ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/reset.py:105
#: paperwork-shell/src/paperwork_shell/cmd/edit.py:122
msgid "Original:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/reset.py:110
msgid "Reseted:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scan.py:47
msgid "Scan pages"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/scan.py:51
msgid "Document to which the scanned pages must be added"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:51
msgid "Commands to manage labels"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:54
msgid "label command"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:61
#: paperwork-shell/src/paperwork_shell/cmd/delete.py:67
msgid "Target documents"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:65
#: paperwork-shell/src/paperwork_shell/cmd/label.py:73
msgid "Target document"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:66
msgid "Label to add"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:69
msgid "Label color (ex: '#aa22cc')"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:74
msgid "Label to remove"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:78
msgid "Label to delete from *all* documents"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:82
msgid "Loading all labels ... "
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/label.py:171
#, python-format
msgid "Are you sure you want to delete label '%s' from all documents ?"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/edit.py:92
msgid "Edit page"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/edit.py:96
msgid "List of image modifiers (comma separated, possible values: {})"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/edit.py:118
msgid "Modifying document {} page {} ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/edit.py:139
msgid "Generating in high quality and saving ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/edit.py:163
msgid "Committing ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/show.py:45
msgid "Show the content of a document"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/show.py:74
#: paperwork-shell/src/paperwork_shell/cmd/search.py:82
#, python-format
msgid "Document id: %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/show.py:80
#: paperwork-shell/src/paperwork_shell/cmd/search.py:87
#, python-format
msgid "Document date: %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/show.py:92
#, python-format
msgid "Page %d"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:64
msgid "Import file(s)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:69
msgid "Target document for import"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:73
msgid "PDF password"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:77
msgid "Files to import"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:104
#, python-format
msgid "Don't know how to import file(s) %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:111
#, python-format
msgid "Found many ways to import file(s) %s."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:114
msgid "Please select the way you want:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:133
msgid "Loading labels ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:149
#, python-format
msgid "Importing %s ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:157
msgid "Import result:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:159
#, python-format
msgid "- Imported files: %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:162
#, python-format
msgid "- Non-imported files: %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:165
#, python-format
msgid "- New documents: %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/import.py:168
#, python-format
msgid "- Updated documents: %s"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:34
msgid "Check and fix work directory integrity"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:38
msgid "Don't ask to fix things, just fix them"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:55
msgid "Checking work directory ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:61
msgid "No problem found"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:65
#, python-format
msgid "%d problems found:"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:80
msgid "- Problem: "
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:84
msgid "- Possible solution: "
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:91
msgid ""
"Do you want to fix those problems automatically using the indicated "
"solutions ?"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:104
msgid "Fixing ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:106
msgid "All fixed !"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/chkworkdir.py:107
#: paperwork-shell/src/paperwork_shell/cmd/sync.py:63
msgid "Synchronizing with work directory ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/delete.py:55
msgid "Delete a document or a page"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/delete.py:60
msgid ""
"Pages to delete (single integer, range or comma-separated list, default: all "
"pages)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/delete.py:83
#, python-brace-format
msgid "Deleting document {doc_id} ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/delete.py:84
#, python-brace-format
msgid "Deleting page {page_idx} of document {doc_id} ..."
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/delete.py:90
#, python-format
msgid "Delete document %s ?"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/delete.py:97
#, python-brace-format
msgid "Delete page(s) {page_indexes} of document {doc_id} ?"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/move.py:54
msgid "Move a page"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/move.py:58
#: paperwork-shell/src/paperwork_shell/cmd/copy.py:50
msgid "Source document"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/move.py:62
msgid "Page to move"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/move.py:66
msgid "Destination document"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/move.py:70
msgid "Target page number"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/copy.py:46
msgid "Copy a document"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/extra_text.py:36
msgid "Manage additional text attached to documents"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/extra_text.py:45
msgid "Get a document additional text"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/extra_text.py:50
msgid "Set a document additional text"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/extra_text.py:74
msgid "No additional text"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/sync.py:53
msgid "Synchronize the index(es) with the content of the work directory"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/search.py:56
msgid "Search keywords in documents"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/search.py:60
msgid "Maximum number of results (default: 50)"
msgstr ""

#: paperwork-shell/src/paperwork_shell/cmd/search.py:64
msgid "Search keywords (none means all documents)"
msgstr ""
